<?php

namespace Drupal\ct_expire;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Psr\Log\LoggerInterface;

/**
 * Service for scheduling cache tags to be expired.
 */
class CtExpireScheduler {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new CtExpireScheduler object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger instance.
   */
  public function __construct(Connection $connection, TimeInterface $time, LoggerInterface $logger) {
    $this->connection = $connection;
    $this->time = $time;
    $this->logger = $logger;
  }

  /**
   * Schedules an item for expiration into 'ct_expire_item' table.
   *
   * @param string $tag
   *   The tag associated with the item to be expired.
   * @param int $timestamp
   *   The UNIX timestamp when the item should be expired.
   * @param string $name
   *   The name of the item to be expired. If empty tag is used.
   *
   * @return bool
   *   TRUE on successful insertion, FALSE otherwise.
   */
  public function schedule(string $tag, int $timestamp, string $name = '') {
    // Clean up name and use tag if no name is given.
    $name = $this->cleanString($name);
    if (empty($name)) {
      $name = $this->cleanString($tag);
    }

    // @todo Do not schedule if timestamp is in the past?
    // @todo Validate tag?
    // Insert expire item.
    if (!empty($name) && !empty($tag)) {
      try {
        $this->connection->merge('ct_expire_item')
          ->key(['name' => $name])
          ->fields([
            'name' => $name,
            'cache_tag' => $tag,
            'expire' => $timestamp,
            'created' => $this->time->getCurrentTime(),
          ])
          ->execute();
      }
      catch (\Exception $e) {
        $this->logger->error(
          'Error creating cache expire tag %name : %error.',
          ['%name' => $name, '%error' => $e->getMessage()]
        );
        return FALSE;
      }

      return TRUE;
    }

    $this->logger->error(
          'Unexpected error creating cache expire tag with name: %name',
          ['%name' => $name]
      );
    return FALSE;
  }

  /**
   * Helper function for special characters from the string.
   */
  private function cleanString(string $string) {
    // Replaces all spaces with hyphens.
    $string = str_replace(' ', '_', $string);
    // Replaces all spaces with hyphens.
    $string = str_replace(':', '_', $string);
    $string = strtolower($string);
    // Removes special chars.
    return preg_replace('/[^A-Za-z0-9\_]/', '', $string);
  }

  /**
   * Retrieves all items from the 'ct_expire_item' table that have expired.
   *
   * @return array
   *   An array of results.
   */
  public function getExpired() {
    return $this->connection->query(
          "SELECT id, cache_tag FROM {ct_expire_item} WHERE :time > expire",
          [':time' => $this->time->getCurrentTime()]
      )->fetchAll();
  }

  /**
   * Removes a specific cache expiration item from the database by its ID.
   *
   * @param int $id
   *   The ID of the cache expiration item to remove.
   *
   * @return bool
   *   TRUE if the item was successfully removed, FALSE otherwise.
   *
   * @throws \Exception
   *   If the deletion query fails.
   */
  public function remove(int $id) {
    try {
      $this->connection->delete('ct_expire_item')
        ->condition('id', $id)
        ->execute();
      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger->error(
            'Error removing cache expire tag. ID: %id : %error.',
            ['%id' => $id, '%error' => $e->getMessage()]
            );
      return FALSE;
    }
  }

}
