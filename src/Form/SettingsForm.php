<?php

namespace Drupal\ct_expire\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure CT expire settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ct_expire_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ct_expire.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['disable_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable cron'),
      '#description' => $this->t('This can be disabled if other methods are being used to invalidate cache tags, i.e. the drush ct_expire:invalidate command.'),
      '#default_value' => $this->config('ct_expire.settings')->get('disable_cron'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ct_expire.settings')
      ->set('disable_cron', $form_state->getValue('disable_cron'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
