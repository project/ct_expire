<?php

namespace Drupal\ct_expire\Commands;

use Drupal\Core\Cache\Cache;
use Drupal\ct_expire\CtExpireScheduler;
use Drush\Commands\DrushCommands;

/**
 * Provides Drush commands for the ct_expire module.
 */
class CtExpireCommands extends DrushCommands {

  /**
   * The CtExpireScheduler service.
   *
   * @var \Drupal\ct_expire\CtExpireScheduler
   */
  protected CtExpireScheduler $scheduler;

  /**
   * CtExpireCommands constructor.
   *
   * @param \Drupal\ct_expire\CtExpireScheduler $scheduler
   *   The scheduler which allows get expired cache tags.
   */
  public function __construct(CtExpireScheduler $scheduler) {
    parent::__construct();
    $this->scheduler = $scheduler;
  }

  /**
   * Invalidate expired cache tags.
   *
   * @command ct_expire:invalidate
   * @aliases ct_invalidate
   *
   * @throws \Exception
   */
  public function invalidate() {
    $cacheExpireTags = $this->scheduler->getExpired();

    // @todo Improve performance by bundling these actions.
    foreach ($cacheExpireTags as $cacheExpireTag) {
      Cache::invalidateTags([$cacheExpireTag->cache_tag]);
      $this->scheduler->remove($cacheExpireTag->id);
    }
  }

}
