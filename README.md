# CT expire

Allow site to set date for a cache tag to expire / invalidate

## Getting started

`$ct_expire = \Drupal::service('ct_expire.scheduler');`
`$ct_expire->schedule('node:12', time(),'node_12_paragraph_21_field_publish_date');`

Use "schedule" function to set a cache tag to be invalidated
on certain timestamp.

Make sure to include a useful name ( see above example ) .
This wil make it easy to trace it back if any complication occurs with it. 

## Notes
* CT expire comes with a form widget that allows you to automatically
  create invalidation items.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

For field widget:
1. In the form display for your entity select "Date and time cache expire" as widget.
2. Save an entity with this type.
3. Make sure cron is running.

## TODO's

* Make drush command to use instead of cron.
* Invalidate all tags at once, instead of for loop. `ct_expire.module:38`
* Prevent scheduling invalidation in the past. 
* Validate given tags.
* Extend default widget to automatically invalidate entity.
* Create Drupal view for current scheduled invalidations
* Create Drupal form to add custom cache tag invalidation.
